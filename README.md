<h1>ТЗ</h1>
<h2>Необходимо реализовать форму обратной связи на Laravel:</h2>
<ul>
<li>регистрация\авторизация: стандартный модуль auth (но пользователи должны быть с двумя ролями: менеджер и клиент.</li>
<li>клиенты регистрируются самостоятельно, а аккаунт менеджера должен быть создан заранее, логин и пароль выслать вместе с готовым заданием)
после логина, клиент видит форму обратной связи, а менеджер список заявок. (все страницы и функционал доступны только авторизованным пользователям и только в соответствии с их привилегиями)
менеджер может просматривать список заявок и отмечать те, на которые ответил.</li>
<li>
список заявок:
<ul>
<li>*ID, тема, сообщение, имя клиента, почта клиента, ссылка на прикрепленный файл, время создания</il>
<li>клиент может оставлять заявку, но не чаще раза в сутки.</li>
<li>на странице создания заявки: тема и сообщение, файловый инпут кнопка “отправить“.</li>
<li>в момент обработки формы и создания заявки отправлять менеджеру email со всеми данными</li>
<li>отправку почты реализовать асинхронно (используя очереди), сделать хотя бы частичное покрытие тестами.</li>
<li>отправку почты реализовать асинхронно (используя очереди), сделать хотя бы частичное покрытие тестами.</li>
</ul>
</li>
<li>На вёрстку внимание обращаться не будет, важно оформление кода, phpdoc, использование фич php7+ и возможностей фреймворка.</li>
</ul>
Каждый раз при изменении чего либо (желательно, чтобы каждый новый функционал был отдельно закомичен) - делать комиты (на англ).
Если будет только 1 комит на все тестовое - не хорошо(
<hr>
<h2>manager credentials:</h2>
<ul>
<li>email: manager@example.com</li>
<li>password: qwerty</li>
</ul>
<h2>user credentials:</h2>
<ul>
<li>email: user@email.com</li>
<li>password: 123456789</li>
</ul>
<hr>
<h2>Info:<h2>
<ul>
<li>PHP 7.1.17</li>
<li>Laravel 5.6</li>
<ul>
<li>Role modules: bican/roles (https://github.com/romanbican/roles)</li>
</ul>
</li>
</ul>