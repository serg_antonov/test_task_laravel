<?php

use Illuminate\Database\Seeder;
use App\User;
use Bican\Roles\Models\Role;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = Role::create([
            'name' => 'Manager',
            'slug' => 'manager',
        ]);

        //$role->attachPermission(Permission::find(1));



        $manager = new User();
        $manager->name = 'manager';
        $manager->email = env('MANAGER_EMAIL');//'email@example.com';
        $manager->password = bcrypt(env('MANAGER_PASSWORD'));//bcrypt('qwerty');
        $manager->save();

        $manager->attachRole($role);

/*




        $view_hits = Permission::create([
            'name' => 'Viewing hits',
            'slug' => 'viewing.hits',
            'description' => '', // optional
        ]);

        $manager_premission = Role::find(1);
        $manager_premission->attachPermission($view_hits);

*/
    }
}
