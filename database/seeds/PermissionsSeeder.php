<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Permission;
use Bican\Roles\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $view_hits = Permission::create([
            'name' => 'Viewing hits',
            'slug' => 'viewing.hits',
            'description' => '', // optional
        ]);

        $manager_premission = Role::find(1);
        $manager_premission->attachPermission($view_hits);
    }
}
