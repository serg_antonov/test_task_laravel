@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Enter your message</div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data">
                        <span class="status-message">{{$message ?? ''}}</span>
                        <input type="text" name="subject" class="form-control" placeholder="Subject">
                        <br>
                        <textarea name="message" class="form-control" placeholder="Message *" required></textarea>
                        <br>
                        <input type="file" name="user_file" class="form-control-file">
                        <br>
                        @csrf
                        <div>* - required field</div>
                        <br>
                        <input type="submit" class="send-message btn btn-primary">
                    </form>
                </div>
                {{--
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
