@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row navbar-laravel card-header">
            <div class="col-sm-1"><b>ID</b></div>
            <div class="col-sm-3"><b>Subject</b></div>
            <div class="col-sm-4"><b>Message</b></div>
            <div class="col-sm-1"><b>User name</b></div>
            <div class="col-sm-1"><b>User email</b></div>
            <div class="col-sm-2"><b>Attachment</b></div>
        </div>
        @foreach($data as $line)
            <div class="row navbar-laravel">
                <div class="col-sm-1">{{$line['id']}}</div>
                <div class="col-sm-3">{{$line['subject']}}</div>
                <div class="col-sm-4">{{$line['message']}}</div>
                <div class="col-sm-1">{{$line['user_name']}}</div>
                <div class="col-sm-1">{{$line['user_email']}}</div>
                <div class="col-sm-2">
                    @if($line['attachment'] != '')
                        <a href="{{$line['attachment']}}">Download</a>
                    @else
                        <span>No attachment</span>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection
