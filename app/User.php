<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use App\Dashboard;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;
//    use Authenticatable, CanResetPassword, HasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**/
    public function dashboard(){
//        return $this->belongsTo(Dashboard::class, 'user_id', 'id');
        return $this->hasMany('App\Dashboard');
    }
    /**/
}
