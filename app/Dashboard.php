<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailToManager;


class Dashboard extends Model
{
    protected $table = 'dashboard';

    protected $fillable = [
        'subject', 'message', 'user_id', 'attachment',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Return view
     *
     * @return string
     */
    public function index(){
        if(isset(Auth::user()->getRoles()->pluck('slug')[0])){
            /*
             * is manager
             * */
            $data = $this->get_all_hits();
            return view('viewing_hits')->with('data', $data);
        }else{
            /*
             * is user
             * */
            if(parent::all()->last() !== null){
                /*
                 * message exists
                 * */
                //dd(Dashboard::find(Auth::id()));
                //if(parent::all()->find(Auth::id())->created_at->diffInDays(Carbon::now()->toDateTimeString()) >= 1){
                if(Dashboard::find(Auth::id()) === null){
                    /*
                     * this is first message
                     * */
                    return response()->view('dashboard', [], 200);
                }else{
                    if(parent::all()->find(Auth::id())->created_at->diffInDays(Carbon::now()->toDateTimeString()) >= 1){
                        /*
                         * last message sended > one day
                         * */
                        return response()->view('dashboard', [], 200);
                    }else{
                        /*
                         * last message sended < one day
                         * */
                        return response()->view('send_error', ['message'=>'Send messages only once per day'], 403);
                    }
                }
            }else{
                /*
                 * message no exists
                 * */
                return response()->view('dashboard', [], 200);
            }
        }
    }

    /**
     * Return view
     *
     * @return string
     */
    public function create($request){

        if($request->input('message') !== null){

            if($request->hasFile('user_file') !== false){
                $path = storage_path().'/app/public/downloads/'.date('Y').'/'.date('m');

                $filename = time().'.'.$request->file('user_file')->getClientOriginalExtension();
                $request->file('user_file')->move($path, $filename);

                $fullpath = $path.'/'.$filename;
            }else{
                $fullpath = '';
            }

            $dashboard = new Dashboard();
            $dashboard->subject = $request->input('subject') ?? 'No subject';
            $dashboard->message = $request->input('message');
            $dashboard->user_id = Auth::id();
            $dashboard->attachment = str_replace('app/storage/app/public/', 'storage/', $fullpath);
            $dashboard->save();

            $last_message = $this->get_all_hits();

            $attach = $fullpath;

            $data = new \stdClass();
            $data->from = '';
            $data->mail_data = $last_message[count($last_message)];
            $data->attachment = $attach;

            $mail_data = new SendMailToManager($data);

            Mail::to(env('MANAGER_EMAIL'))->send($mail_data);

            return response()->view('dashboard', [], 200);

        }else{
            return response()->view('dashboard', [], 204);
        }
    }

    /**
     * Select data all hits
     *
     * @return mixed
     */
    private function get_all_hits(){
        $result = array();
        for($i=1; $i<=count(parent::get()); $i++){
            $result[$i]['id'] = parent::find($i)->id;
            $result[$i]['subject'] = parent::find($i)->subject;
            $result[$i]['message'] = parent::find($i)->message;
            $result[$i]['user_name'] = parent::find($i)->user()->first()->name;
            $result[$i]['user_email'] = parent::find($i)->user()->first()->email;
            $result[$i]['attachment'] = parent::find($i)->attachment;
        }
        return $result;
    }
}
