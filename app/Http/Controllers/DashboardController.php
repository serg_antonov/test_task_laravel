<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboard;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Return view
     *
     * @return string
     */
    public function index(){
        $dashboard = new Dashboard();
        return $dashboard->index();
    }

    /**
     * Add message to dashboard
     *
     * @var request Request Laravel Request
     * @return void
     */
    public function add(Request $request){
        $dashboard = new Dashboard();
        return $dashboard->create($request);
    }
}
